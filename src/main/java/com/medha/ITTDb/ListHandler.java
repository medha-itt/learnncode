package com.medha.ITTDb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListHandler {
	public static <T> List<T> filterObjectList(String key, String value, List<T> fileObjects) throws ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		ArrayList<T> objectsFound = new ArrayList<T>();
			for (int index =0 ; index< fileObjects.size(); index++) {
				T object = fileObjects.get(index);
				String objFieldValue = ""; 
				objFieldValue = object.getClass().getDeclaredField(key).get(object).toString();
				if (objFieldValue.contains(value)) {
					objectsFound.add(object);
				}
		}
			
		return objectsFound;
	}
	
	public static <T> List<T> searchObject(List<T> objectsList, T object) throws ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		ArrayList<T> objectsFound = new ArrayList<T>();
		for (int objIndex = 0; objIndex < objectsList.size(); objIndex++) {
			T tempObject = objectsList.get(objIndex);
			if (tempObject.getClass().getDeclaredField("id").get(tempObject).equals(object.getClass().getDeclaredField("id").get(object)))
			{
				objectsFound.add(tempObject);
			}
		}
		System.out.println(objectsFound.toString());
		return objectsFound;
	}
	
	public static <T> T removeObject(String fileName, List<T> objects, T objectToBeDeleted) throws ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, IOException {
		T removedObject = null;
		for (int objIndex = 0; objIndex < objects.size(); objIndex++) {
			T tempObj = objects.get(objIndex);
			String tempObjProperty = tempObj.getClass().getDeclaredField("id").get(tempObj).toString();
			String deleteObjectProperty = objectToBeDeleted.getClass().getDeclaredField("id").get(objectToBeDeleted).toString();
			if (tempObjProperty.equals(deleteObjectProperty)) {
				removedObject = objects.remove(objIndex);
			}
			if (removedObject != null)
			{
				FilesHandler.write(fileName, objects);
			}
		}
		return removedObject;
	}
}
