package com.medha.ITTDb;
import java.lang.Exception;

public class ITTDbExceptions extends Exception {
	private static final long serialVersionUID = 1L;
	String errorMessage;

	public ITTDbExceptions(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public void printMessage() {
		System.out.println("Error occured:" + errorMessage);
	}
}