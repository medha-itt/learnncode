package com.medha.fileChecker;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataChecker {
	public <T> void checkFileData(String actualList, List<T> updatedList) throws JsonParseException, JsonMappingException, IOException
	{
		Reader reader = new StringReader(actualList);
		ObjectMapper objectMapper = new ObjectMapper();
		List<T> list = (List<T>) objectMapper.readValue(reader, Type.class);
		
	}
}
