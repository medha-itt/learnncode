package com.medha.ITTDbTest;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.medha.ITTDb.Customer;
import com.medha.ITTDb.ITTDatabase;
import com.medha.ITTDb.ITTDbExceptions;
import com.medha.fileChecker.DataChecker;

public class UpdateTest {
	File file;
	DataChecker dataChecker = new DataChecker();
    @Before
    public void initialize() throws IOException {
        file = new File("Customer.json");
		FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[{\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"}]");
        fileWriter.close();
    }

    @Test
    public <T> void updateTest() throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
//         Arrange
    	Customer testCustomer = new Customer(1, "Medha", "Lucknow");
    	List<T> fileObjects = new ArrayList<T>();
//         Act
    	boolean isUpdated = ITTDatabase.update(testCustomer);
//         Assert
      assertEquals(true, isUpdated);

    }

    @After
    public void cleanup() throws IOException {
        PrintWriter writer = new PrintWriter("Customer.json");
        writer.print("");
        writer.close();
    }
}