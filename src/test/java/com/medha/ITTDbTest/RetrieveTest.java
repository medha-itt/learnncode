package com.medha.ITTDbTest;
import static org.junit.Assert.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import com.medha.ITTDb.Customer;
import com.medha.ITTDb.ITTDatabase;
import com.medha.ITTDb.ITTDbExceptions;

import org.junit.Before;
import org.junit.After;

public class RetrieveTest {
	ITTDatabase ittCRUD = new ITTDatabase();
	File file;
    @Before
    public void initialize() throws IOException {
        file = new File("Customer.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[{\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"}]");
        fileWriter.close();
    }

	@SuppressWarnings("unchecked")
	@Test
    public <T> void retrieveTest() throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        Customer testCustomer = new Customer(1,"Neha","Lucknow");
        List<T> retrievedObjects = (List<T>) ittCRUD.retrieve(testCustomer, "name", "Neha");
        List<T> actualList = new ArrayList<T>();
        actualList.add((T)testCustomer);
        
        assertEquals(true, retrievedObjects.size()>0);
    }

    @After
    public void cleanup() throws IOException {;
        file.delete();
    }
}