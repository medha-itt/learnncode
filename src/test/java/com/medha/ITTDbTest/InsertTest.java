package com.medha.ITTDbTest;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.*;

import com.medha.ITTDb.Customer;
import com.medha.ITTDb.ITTDatabase;
import com.medha.ITTDb.ITTDbExceptions;

public class InsertTest {
	ITTDatabase ittCRUD = new ITTDatabase();
	File file = null;
    @Before
    public void initialize() throws IOException {
        file = new File("Customer.json");
    }

        @Test
        public void insertTest() throws IOException, ITTDbExceptions {
                // Arrange
                Customer testCustomer = new Customer(1, "Neha", "Lucknow");
                // Act
                ittCRUD.insert(testCustomer);
                // Assert
                assertEquals(63, file.length());
        }

        @After
        public void cleanup() throws IOException {
        	file.delete();
        }

}