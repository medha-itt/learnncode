package com.medha.ITTDbTest;

import java.io.*;
import org.junit.Before;
import org.junit.Test;

import com.medha.ITTDb.Customer;
import com.medha.ITTDb.ITTDatabase;
import com.medha.ITTDb.ITTDbExceptions;

import static org.junit.Assert.*;

public class DeleteTest {
	File file = null;
    @Before
    public void initialize() throws IOException {
        file = new File("Customer.json");
		FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[ {\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"} ]");
        fileWriter.close();
    }

    @Test
    public void retrieveTest() throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        Customer testCustomer = new Customer(1, "Medha", "Lucknow");
        ITTDatabase.delete(testCustomer, "id", "1");
        assertEquals(true,file.length()<=3);
    }
}